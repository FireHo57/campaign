﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyComponent : MonoBehaviour, ActionInterface
{
    System.Action m_callback;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // do nothing
    }

    public void Run()
    {
        // do nothing just return done
        m_callback();
    }

    public void SetCallback(System.Action doneCallback)
    {
        m_callback = doneCallback;
    }
}
