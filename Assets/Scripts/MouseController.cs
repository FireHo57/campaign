﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class MouseController : MonoBehaviour
{
    private List<GameObject> MouseHandlerTargets;

    void Awake()
    {
        MouseHandlerTargets = new List<GameObject>();
    }

    public void RegisterForMouseEvents(GameObject target)
    {
        MouseHandlerTargets.Add(target);
    }
    
    // Update is called once per frame
    void Update()
    {
        if( MouseHandlerTargets.Count != 0 && !IsPointerOverUIElement() )
        {
            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPos.z=0;

            int mouseButton = -1;
            if (Input.GetMouseButton(0))
            {
                mouseButton = 0;
            }
            else if (Input.GetMouseButton(1))
            {
                mouseButton = 1;
            }
            else if (Input.GetMouseButton(2))
            {
                mouseButton = 2;
            }

            foreach(GameObject go in MouseHandlerTargets)
            {
                ExecuteEvents.Execute<MouseHandler>(go,null,(x,y)=>x.OnMousePosition(mouseWorldPos));
                if( mouseButton != -1 )
                {
                    ExecuteEvents.Execute<MouseHandler>(go,null, (x,y)=>x.OnMouseClick(mouseButton));
                }
            }
        }
        
    }

    public static bool IsPointerOverUIElement()
    {
        var eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);
        return results.Count > 0;
    }

}
