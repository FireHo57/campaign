﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface GridRequestHandler : IEventSystemHandler
{
    void SetAtCoordinates(GameObject targetToSet, Vector3Int position);
    void RedrawCells( List<Vector3Int> CellsToRedraw );
}
