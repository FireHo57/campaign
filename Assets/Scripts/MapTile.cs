﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

enum SpriteReference
{
    BASE = 0,
    HIGHLIGHT = 1,
    DEBUG_HIGHLIGHT = 2,
    SELECTION_HIGHLIGHT = 3,
    MOVE_HIGHLIGHT = 4
}

[CreateAssetMenu]
public class MapTile : TileBase
{
    public Tile.ColliderType colliderType = Tile.ColliderType.Grid;

    public Sprite[] m_Sprites;

    [SerializeField]
    Sprite m_preview;

    [SerializeField]
    int m_moveCost=1;

    public int h_cost=0;
    public int g_cost=0;

    public int fCost()
    {
        return g_cost + h_cost;
    }

    [SerializeField]
    public Vector3 m_drawOffset;

    public Vector3Int m_tilemapPosition;

    bool m_highlight = false;

    bool m_debughighlight=false;

    public MapTile parent;

    int currentHighlight = 0;
    int previousHighlight = 0;

    public void SetTilemapPosition(Vector3Int position)
    {
        m_tilemapPosition = position;
    }

    public void Highlight(bool highlight)
    {
        if( highlight )
        {
            int target = (int)SpriteReference.HIGHLIGHT;
            previousHighlight = currentHighlight;
            currentHighlight = target;
        }
        else
        {
            SetPreviousHighlight();
        }       
    }

    public void DebugHighlight(bool debug_highlight)
    {
        if(debug_highlight)
        {
            int target = (int)SpriteReference.DEBUG_HIGHLIGHT;
            previousHighlight = currentHighlight;
            currentHighlight = target;
        }
        else
        {
            int target = (int)SpriteReference.BASE;
            previousHighlight = currentHighlight;
            currentHighlight = target;
        }    
    }

    public void MoveHighlight(bool move_highlight)
    {
        if(move_highlight)
        {
            int target = (int)SpriteReference.MOVE_HIGHLIGHT;
            previousHighlight = currentHighlight;
            currentHighlight = target;
        }
        else
        {
            int target = (int)SpriteReference.BASE;
            previousHighlight = currentHighlight;
            currentHighlight = target;
        }    
    }

    public void SetPreviousHighlight()
    {
        int tempHighlight = currentHighlight;
        currentHighlight = previousHighlight;
        previousHighlight = tempHighlight;
    }

    

    public bool IsHighTile()
    {
        return m_tilemapPosition.y%2 != 0;
    }

    public Vector3Int[] GetAdjacentPositions()
    {
        //hexgrid so adjacent positions are 2 above, 2 to either side and 2 below
        //start top left and move clockwise
        Vector3Int[] positions = new Vector3Int[6];

        //top and bottom
        positions[0] = new Vector3Int(m_tilemapPosition.x+1,m_tilemapPosition.y,0);
        positions[1] = new Vector3Int(m_tilemapPosition.x-1,m_tilemapPosition.y,0);
        //top left and top right
        positions[2] = new Vector3Int(m_tilemapPosition.x,m_tilemapPosition.y+1,0);
        positions[3] = new Vector3Int(m_tilemapPosition.x,m_tilemapPosition.y-1,0);
        // bottom left and bottom right
        if( IsHighTile() )
        {
            positions[4] = new Vector3Int(m_tilemapPosition.x+1, m_tilemapPosition.y+1,0);
            positions[5] = new Vector3Int(m_tilemapPosition.x+1, m_tilemapPosition.y-1,0); 
        }
        else
        {
            positions[4] = new Vector3Int(m_tilemapPosition.x-1,m_tilemapPosition.y+1,0);
            positions[5] = new Vector3Int(m_tilemapPosition.x-1,m_tilemapPosition.y-1,0);     
        }
        
        return positions;
    }

    public List<Vector3Int> HighlightAvailableMoves(int moveDistance)
    {
        GameObject g = GameObject.FindGameObjectWithTag("Map");
        Tilemap tilemap = g.GetComponentsInChildren<Tilemap>()[0];
        Vector3Int[] tilePositions = GetAdjacentPositions();
        List<Vector3Int> availableMoves = new List<Vector3Int>();
        foreach( Vector3Int position in tilePositions)
        {
            MapTile mt = (MapTile)tilemap.GetTile(position);
            if( mt != null )
            {
                int moveCost = mt.GetMoveCost();
                if(moveDistance > moveCost)
                {
                    Debug.Log("Tile position highlighted: " + position);
                    mt.DebugHighlight(true);
                    availableMoves.Add(position);
                    mt.HighlightAvailableMoves(moveDistance-moveCost);
                }
                else if( moveDistance == moveCost )
                {
                    mt.MoveHighlight(true);
                    availableMoves.Add(position);
                }
            }
        }

        return availableMoves;
    }

    public int GetMoveCost()
    {
        return m_moveCost;
    }

    public override void RefreshTile( Vector3Int location, ITilemap tilemap )
    {
        tilemap.RefreshTile(location);
    }

    public override void GetTileData(Vector3Int location, ITilemap tilemap, ref TileData tileData)
    {
        if( currentHighlight >= m_Sprites.Length )
        {
            Debug.Log("Sprites size: "+m_Sprites.Length+" currentHighlight: "+currentHighlight);
        }

        tileData.sprite = m_Sprites[currentHighlight];
        
        
        if( m_debughighlight )
        {
            tileData.sprite = m_Sprites[(int)SpriteReference.DEBUG_HIGHLIGHT];
            return;
        }

        if( m_highlight )
        {
            tileData.sprite = m_Sprites[(int)SpriteReference.HIGHLIGHT];
        }
    }

}
