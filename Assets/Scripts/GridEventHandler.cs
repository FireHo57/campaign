﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface GridEventHandler : IEventSystemHandler
{
    void OnCoordinateChange(Vector3Int newCoordinate);
    void OnMouseClick(Vector3Int clickCoordinate);
}
