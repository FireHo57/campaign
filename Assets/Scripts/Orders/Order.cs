using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class wraps up order actions, order ui (if any) and a reference back to the unit
public class Order : MonoBehaviour
{

    public GameObject m_startAction;
    public GameObject m_duringAction;
    public GameObject m_endAction;

    private bool m_startDone = false;
    private bool m_duringDone = false;
    private bool m_endDone = false;

    private System.Action<GameObject> m_doneCallback;

    public void SetDoneCallback(System.Action<GameObject> callback)
    {
        m_doneCallback = callback;
    }

    public void SetStartAction(GameObject startAction)
    {
        m_startAction = startAction;
        m_startAction.transform.parent = transform;
        m_startAction.GetComponent<ActionInterface>().SetCallback(SetStartDone);
    }

    public void SetDuringAction(GameObject duringAction)
    {
        m_duringAction = duringAction;
        m_duringAction.transform.parent = transform;
        m_duringAction.GetComponent<ActionInterface>().SetCallback(SetDuringDone);
    }

    public void SetEndAction(GameObject endAction)
    {
        m_endAction = endAction;
        m_endAction.transform.parent = transform;
        m_endAction.GetComponent<ActionInterface>().SetCallback(SetEndDone);
    }

    private void SetStartDone()
    {
        m_startDone = true;
        Done();
    }

    private void SetDuringDone()
    {
        m_duringDone = true;
        Done();
    }

    private void SetEndDone()
    {
        m_endDone = true;
        Done();
    }

    private void Done()
    {
        if( m_startDone && m_duringDone && m_endDone )
        {
            m_doneCallback(gameObject);
        }
    }
}