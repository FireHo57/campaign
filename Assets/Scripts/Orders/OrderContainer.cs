using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
An OrderContainer exists to keep togethor all the different parts of an order.
1) graphical elements
2) actions related to the order
3) construction functions for actions that cannot be created yet
etc

This also helps order factory return something when construction various order types.
*/
public class OrderContainer
{

public enum ACTION_STAGE
{
    START=0,
    RUN = 1,
    END=2
}

public Actions.Action StartAction;
public Actions.Action RunAction;
public Actions.Action EndAction;


private Order m_order;
private bool m_orderComplete;
public OrderContainer(Actions.Action startAction, Actions.Action duringAction, Actions.Action endAction)
{
    this.StartAction = startAction;
    this.RunAction = duringAction;
    this.EndAction = endAction;

    m_orderComplete = false;
}

public void SetDefferedStartAction(Actions.Action defferedAction)
{
    SetDefferedAction(defferedAction, ACTION_STAGE.START);
}

private void SetDefferedAction(Actions.Action delayedAction, ACTION_STAGE stage)
{
    switch (stage)
    {
        case ACTION_STAGE.START:
            StartAction = delayedAction;
            break;
        case ACTION_STAGE.RUN:
            RunAction = delayedAction;
            break;
        case ACTION_STAGE.END:
            EndAction = delayedAction;
            break; 
    }
}



}