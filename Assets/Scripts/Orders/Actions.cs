using UnityEngine;

// this file contains the various action 'types' that can be performed and their construction functions
public class Actions
{
    public delegate Vector3Int VectorReturner();
    public delegate void Action();
    
    public delegate int AttributeModifier(int startingAttribute);

    public static void MoveAction(GameObject target, Vector3Int endGridPosition)
    {
       
    }

    // Makes a move action. This should be passed to 
    static Action MoveActionConstructor(GameObject target, Vector3Int endGridPosition)
    {      
        Action moveAction = () =>
        {
            MoveAction(target, endGridPosition);
        };

        return moveAction;
    }

    static void ChangeAttribute(GameObject target, Unit.Attributes attribute, AttributeModifier changeFunc)
    {
        Debug.Log("Changing the attribute: "+attribute+" on unit "+target);
    }
}
