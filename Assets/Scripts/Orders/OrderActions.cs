﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
This class represents an order that can be run by the order runner
    
    Orders Have 3 phases they can affect.
    Things that take place at the begginning of the turn,
    things that take place during the turn and 
    things that take place at the end

*/
public class OrderActions  
{
    public enum ACTION_STAGE
    {
        START=0,
        RUN = 1,
        END=2
    }
    public delegate void Action();
    public OrderActions(
        Action StartAction,
        Action RunAction,
        Action EndAction
    )
    {
        this.StartAction = StartAction;
        this.RunAction = RunAction;
        this.EndAction = EndAction;    
    }

    public Action StartAction;
    public Action RunAction;
    public Action EndAction;

    public void SetDefferedAction(Action delayedAction, ACTION_STAGE stage)
    {
        switch (stage)
        {
            case ACTION_STAGE.START:
                StartAction = delayedAction;
                break;
            case ACTION_STAGE.RUN:
                RunAction = delayedAction;
                break;
            case ACTION_STAGE.END:
                EndAction = delayedAction;
                break; 
        }
    }

    //public void SetDefferedActionConstructor
}
