﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderFactory : MonoBehaviour
{
    public delegate GameObject OrderMaker(GameObject target);
    private Dictionary<string, OrderMaker> m_makers;
    private GameObject m_grid;

    public delegate void Action();

    public static OrderFactory instance = null;

    [SerializeField]
    GameObject MoveComponent;
    [SerializeField]
    GameObject EmptyComponent;

    [SerializeField]
    GameObject OrderPrefab;

    public void Awake()
    {
        m_makers = new Dictionary<string, OrderMaker>();
        m_makers.Add("Move", MakeMoveOrder);
        m_grid = GameObject.FindWithTag("GridController");
    }

    public GameObject MakeOrder(string orderName, GameObject OrderTarget)
    {
        Debug.Log("Make Order");
        OrderMaker maker = m_makers[orderName];
        if(maker == null)
        {
            Debug.Log("Order "+orderName+" doesn't exist");
            return null;
        }
        else
        {
            return maker(OrderTarget);
        }
    }

    //Empty function, it does nothing
    public void Empty()
    {

    }

    private GameObject MakeMoveOrder(GameObject targetUnit)
    {
        //start position
        Unit unit = targetUnit.GetComponent<Unit>();
        Vector3Int startPosition = unit.CurrentPosition();

        GameObject orderObject = Instantiate(OrderPrefab);
        Order newOrder = orderObject.GetComponent<Order>();
        newOrder.SetStartAction(Instantiate(EmptyComponent));
        newOrder.SetDuringAction(Instantiate(MoveComponent));
        newOrder.SetEndAction(Instantiate(EmptyComponent));

        newOrder.m_duringAction.GetComponent<MoveUI>().Init(targetUnit, m_grid);

        //this is the action that gets called once the grid has finished
        GridController gc = m_grid.GetComponent<GridController>();
        gc.GetMove(unit.MoveValue(), startPosition, delegate (Vector3Int endPosition)
        {
            newOrder.m_duringAction.GetComponent<MoveUI>().Fix();
            newOrder.m_duringAction.GetComponent<MoveAction>().Init(targetUnit, endPosition);
        });

        return orderObject;
    }
    
}
