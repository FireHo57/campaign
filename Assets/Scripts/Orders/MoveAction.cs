﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*This class defines what happens on a move action
 e.g. (1) The move is checked with the unit controller
      (2) The graphical move happens (i.e. the unit moves from a to b)
      (3) The move completes and the order is deleted (not this classes responsibility)
*/
public class MoveAction : MonoBehaviour, ActionInterface
{
    private GameObject m_targetUnit;
    private GridController m_grid;
    private GameObject m_uc;

    private bool m_run=false;
    private float m_startTime;
    private float m_journeyLength;
    private Vector3 m_startPosition;
    private Vector3Int m_endPosition;
    private System.Action m_doneCallback;

    public void Init(GameObject target, Vector3Int endPosition)
    {
        m_targetUnit = target;
        m_endPosition = endPosition;
        m_grid = GameObject.FindWithTag("GridController").GetComponent<GridController>();
        m_uc = GameObject.FindWithTag("UnitController");
        if( m_uc == null )
        {
            Debug.Log("m_uc null");
        }
        
    }

    public void Run()
    {
        m_run = true;
        m_startTime = Time.time;
        m_startPosition = m_targetUnit.transform.position;
        Vector3 endPosition = m_grid.GetWorldCoordinates(m_endPosition);
        m_journeyLength = Vector3.Distance(m_startPosition, endPosition);
        Vector3Int gridStartPosition = m_grid.GetCellCoordinates(m_startPosition);
        UnitController uc = m_uc.GetComponent<UnitController>();
        uc.MoveUnit(gridStartPosition, m_endPosition);
        MoveUI mui = GetComponentInParent<MoveUI>();
        mui.HideAll();
    }

    public void SetCallback(System.Action doneCallback)
    {
        m_doneCallback = doneCallback;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_run)
        {
            // Distance moved equals elapsed time times speed..
            float distCovered = (Time.time - m_startTime) * m_targetUnit.GetComponent<Unit>().MoveValue();

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / m_journeyLength;

            // Set our position as a fraction of the distance between the markers.
            m_targetUnit.transform.position = Vector3.Lerp(m_startPosition, m_grid.GetWorldCoordinates(m_endPosition), fractionOfJourney);
            if( m_targetUnit.transform.position == m_grid.GetWorldCoordinates(m_endPosition))
            {
                m_run = false;
                m_doneCallback();
            }
        }
    }
}
