using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*
This class is responsible for running orders 
*/
public class OrderRunner : MonoBehaviour
{

    List<GameObject> m_orders;

    void Start()
    {
        m_orders = new List<GameObject>();
    }

    public void AddOrder(GameObject newOrder)
    {
        m_orders.Add(newOrder);
        newOrder.GetComponent<Order>().SetDoneCallback(UpdateDone);
    }

    public void RunOrders()
    {
        UnitController uc = GameObject.Find("UnitController").GetComponent<UnitController>();
        List<GameObject> orders = uc.GetUnitOrders();
        foreach (GameObject o in orders)
        {
            o.GetComponent<Order>().m_startAction.GetComponent<ActionInterface>().Run();
        }
        foreach(GameObject o in orders)
        {
            o.GetComponent<Order>().m_duringAction.GetComponent<ActionInterface>().Run(); ;
        }
        foreach(GameObject o in orders)
        {
            o.GetComponent<Order>().m_endAction.GetComponent<ActionInterface>().Run(); ;
        }
    }

    private void UpdateDone(GameObject orderDone)
    {
        m_orders.RemoveAll(o => o.GetInstanceID() == orderDone.GetInstanceID());
        Destroy(orderDone);
        if(m_orders.Count() == 0)
        {
            GameObject.Find("UnitController").GetComponent<UnitController>().ClearUnitOrders();
        }
    }

    void Update()
    {
        if( Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Running orders");
            RunOrders();
        }
    }

}