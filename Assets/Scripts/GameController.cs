﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameController : MonoBehaviour
{
    //For building the grid
    [SerializeField]
    Grid GridPrefab;

    [SerializeField]
    TileBase[] TilePrefabs;

    [SerializeField]
    Vector3Int GridMinimums;

    [SerializeField]
    Vector3Int GridMaximums;


    [SerializeField]
    GameObject  UnitControllerPrefab;    

    [SerializeField]
    GameObject mouseController;

    [SerializeField]
    Vector3Int m_unitPosition;

    [SerializeField]
    GameObject GridControllerPrefab;


    [SerializeField]
    GameObject CanvasPrefab;

    [SerializeField]
    GameObject UnitCardPrefab;

    [SerializeField]
    GameObject CanvasManagerPrefab;

    [SerializeField]
    GameObject OrderFactory;

    void Awake()
    {
        Instantiate(CanvasManagerPrefab);

        GridBuilder.CreateMap(GridMinimums, GridMaximums, GridPrefab, TilePrefabs);

        //make the mouse controller.
        //This needs to be made first so things can be registered with it.
        GameObject mouse = Instantiate(mouseController);

        // make the grid
        //Instantiate(grid);
        GameObject gridObj = GameObject.FindWithTag("Map");
        Grid grid_actual = gridObj.GetComponent<Grid>();
        Tilemap tm = gridObj.GetComponentInChildren<Tilemap>();

        GameObject grid_controller = Instantiate(GridControllerPrefab);
        grid_controller.tag = "GridController";
        grid_controller.GetComponent<GridController>().Init(grid_actual);
        grid_controller.GetComponent<GridController>().RegisterGridChangeTarget(grid_controller);
        //make the unit controller
        GameObject unitController_go = Instantiate(UnitControllerPrefab);
        unitController_go.name = "UnitController";
        unitController_go.GetComponent<UnitController>().init(tm.size, tm.cellBounds);
        grid_controller.GetComponent<GridController>().RegisterGridChangeTarget(unitController_go);
        grid_controller.GetComponent<AStar>().Init();

        MouseController mouse_c = mouse.GetComponent<MouseController>();
        mouse_c.RegisterForMouseEvents(grid_controller);

        //Order factory automatic
        GameObject of = GameObject.Instantiate(OrderFactory);
        of.name = "OrderFactory";
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
