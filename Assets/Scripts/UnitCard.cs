﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(CanvasRenderer))]
[RequireComponent(typeof(Image))]
public class UnitCard : MonoBehaviour 
{
    [SerializeField]
    GameObject ButtonPrefab;

    [SerializeField]
    GameObject TitlePrefab;

    public void Create(GameObject unitObject)
    {
        //Get the order factory
        OrderFactory of = GameObject.Find("OrderFactory").GetComponent<OrderFactory>();

        VerticalLayoutGroup layout = gameObject.GetComponent<VerticalLayoutGroup>();

        // make the title        
        GameObject title = Instantiate(TitlePrefab, new Vector3(0,0,0), Quaternion.identity, transform);
        Text title_text = title.GetComponent<Text>();
        title_text.text = unitObject.GetComponent<Unit>().GetName();
        title_text.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

        List<string> orders = unitObject.GetComponent<Unit>().GetAvailableOrders();   
        foreach(string order in orders)
        {
            GameObject button = Instantiate(ButtonPrefab, new Vector3(0,0,0), Quaternion.identity, transform);
            button.name = order+"Button";
            Text button_text = button.GetComponentInChildren<Text>();
            button_text.text = order;
            Button actual = button.GetComponent<Button>();
            actual.onClick.AddListener(delegate 
            {
                //remove units current order
                Unit unitScript = unitObject.GetComponent<Unit>();
                unitScript.SetOrder(of.MakeOrder(order, unitObject));
            });
        }
    }


}
