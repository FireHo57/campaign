﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class UnitController : MonoBehaviour, GridEventHandler
{
    private List<GameObject> currentSelection=null;

    private Dictionary<Vector3Int, GameObject> m_unitsByPositions;
    private Dictionary<GameObject,  Vector3Int> m_positionsByUnits;
    private List<GameObject> m_units;
    private Dictionary<Vector3Int, GameObject> controlZones;

    private GameObject m_gridController;

    private bool m_running = false;

    [SerializeField]
    GameObject unitPrefab;

    GameObject m_canvas;

    State currentState;

    enum State
    {
        NO_SELECTION = 0,
        SELECTION = 1,
        ORDERING = 2
    }

    //Dictionary<string, System.Func<GameObject,bool>> OrderTable;
    Dictionary<string, GameObject> OrderTable;

    public void Start()
    {
        GameObject unit = Instantiate(unitPrefab);
        Debug.Log("Unit at: "+unit.transform.position);
        RegisterUnit(unit, new Vector3Int(0,1,0));
        Debug.Log("Unit now at: "+unit.transform.position);
        m_canvas = GameObject.Find("Canvas");
        currentState = State.NO_SELECTION;
    }

    public void init(Vector3Int size, BoundsInt cellBounds)
    {
        m_gridController = GameObject.FindWithTag("GridController");
        m_unitsByPositions = new Dictionary<Vector3Int, GameObject>();
        m_positionsByUnits = new Dictionary<GameObject, Vector3Int>();
        currentSelection = new List<GameObject>();
        m_units = new List<GameObject>();
        int x_size = size.x;
        int y_size = size.y;
        if( cellBounds.xMin < 0 )
        {
            x_size = size.x/2;
        }
        if(cellBounds.yMin < 0)
        {
            y_size = size.y/2;
        }

        for( int x = cellBounds.xMin; x < x_size; x++)
        {
            for(int y = cellBounds.yMin; y < y_size; y++)
            {
                m_unitsByPositions.Add(new Vector3Int(x,y,0), null);
            }
        }
    }

    public bool Select(Vector3Int mapPosition)
    {
        GameObject selectedUnit = m_unitsByPositions[mapPosition];

        if( selectedUnit!= null)
        {
            if(currentSelection.Contains(selectedUnit))
            {
                //deselect the current selection
                currentSelection.Remove(selectedUnit);
                ExecuteEvents.Execute<UnitCardReceiver>(m_canvas, null, (x,y)=>x.HideUnitCards());
                return false;
            }
            else
            {
                // Add to the current selection
                currentSelection.Add(selectedUnit);

                //put the unit card into the canvas
                GameObject card = selectedUnit.GetComponent<Unit>().GetCard();
                ExecuteEvents.Execute<UnitCardReceiver>(m_canvas, null, (x,y)=>x.ShowUnitCard(card,gameObject));
                return true;
            }
        }
        else
        {
            // no selection so clear selection
            currentSelection.Clear();
            ExecuteEvents.Execute<UnitCardReceiver>(m_canvas, null, (x,y)=>x.HideUnitCards());
            return false;
        }
    }

    public bool RegisterUnit(GameObject unit, Vector3Int mapPosition)
    {
        if( m_unitsByPositions[mapPosition] != null )
        {
            return false;
        }
        m_unitsByPositions[mapPosition] = unit;
        m_positionsByUnits[unit] = mapPosition;
        m_units.Add(unit);
        ExecuteEvents.Execute<GridRequestHandler>(m_gridController, null, (x,y)=>x.SetAtCoordinates(unit ,mapPosition));

        unit.GetComponent<Unit>().SetPosition(mapPosition);
        return true;
    }

    public bool MoveUnit(Vector3Int oldPosition, Vector3Int newPosition)
    {
        if(m_unitsByPositions[newPosition] != null)
        {
            return false;            
        }
        if( m_unitsByPositions[oldPosition] == null )
        {
            return false;
        }

        GameObject movingUnit = m_unitsByPositions[oldPosition];
        m_unitsByPositions[newPosition] = movingUnit;
        m_unitsByPositions[oldPosition] = null;
        m_positionsByUnits[movingUnit] = newPosition;
        movingUnit.GetComponent<Unit>().SetPosition(newPosition);
        return true;
    }

    public void Run()
    {
        m_running = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_running)
        {

        }
    }

    public List<GameObject> GetUnitOrders()
    {
        List<GameObject> orders = new List<GameObject>();
        foreach(GameObject unit in m_units)
        {
            orders.Add(unit.GetComponent<Unit>().m_order);
        }
        return orders;
    }

    public void ClearUnitOrders()
    {
        foreach (GameObject unit in m_units)
        {
            unit.GetComponent<Unit>().RemoveOrder();
        }
    }

    public void OnCoordinateChange(Vector3Int newCoord)
    {
        return;
    }

    public void OnMouseClick(Vector3Int clickPos)
    {
        Select(clickPos);
    }

}
