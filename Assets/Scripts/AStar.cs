﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar : MonoBehaviour, GridEventHandler
{
    LineRenderer m_lr;

    public void OnMouseClick(Vector3Int pos)
    {

    }

    public void OnCoordinateChange(Vector3Int pos)
    {
        /*
        FindPath(new Vector3Int(0, 0, 0), pos);
        List<Vector3> path = retracePath(m_gc.GetTile(new Vector3Int(0, 0, 0)), m_gc.GetTile(pos));
        m_lr.positionCount = 0;
        m_lr.positionCount = path.Count;
        Debug.Log(path.ToString());
        m_lr.SetPositions(path.ToArray());
        */
    }


    GridController m_gc;

    public List<Vector3> GetPath(Vector3Int start, Vector3Int end)
    {
        Debug.Log("aStar start: " + start + " end: " + end);
        FindPath(start, end);
        return retracePath(m_gc.GetTile(start), m_gc.GetTile(end));
    }

    public void Init()
    {
        m_gc = GetComponent<GridController>();
        m_lr = new GameObject().AddComponent<LineRenderer>();
        m_lr.startColor = Color.grey;
        m_lr.endColor = Color.grey;
        m_lr.startWidth = 0.2f;
        m_lr.endWidth = 0.2f;
        
        Color fadedRed = new Color();
        fadedRed = Color.red;
        fadedRed.a = 0.5f;
        //you can have colour gradients on lines but for now I just want a block colour
        m_lr.startColor = fadedRed;
        m_lr.endColor = fadedRed;
        m_lr.material = new Material(Shader.Find("Sprites/Default"));
        // Need this next line because otherwise the line draws behind the grid
        /*MAGIC NUMBER ALERT*/
        m_lr.sortingOrder = 1;
    }


    void FindPath(Vector3Int start, Vector3Int end)
    {
        MapTile startTile = m_gc.GetTile(start);
        MapTile endTile = m_gc.GetTile(end);

        List<MapTile> openSet = new List<MapTile>();
        HashSet<MapTile> closedSet = new HashSet<MapTile>();

        openSet.Add(startTile);

        while(openSet.Count > 0)
        {
            MapTile currentTile = openSet[0];
            for(int i = 1; i < openSet.Count; i++)
            {
                if( openSet[i].fCost() < currentTile.fCost() || 
                    openSet[i].fCost() == currentTile.fCost() && openSet[i].h_cost < currentTile.h_cost)
                {
                    currentTile = openSet[i];
                }
            }

            openSet.Remove(currentTile);
            closedSet.Add(currentTile);

            Vector3Int[] adjacents = currentTile.GetAdjacentPositions();
            foreach(Vector3Int pos in adjacents)
            {
                MapTile mt = m_gc.GetTile(pos);
                if (mt == null)
                    continue;

                if (closedSet.Contains(mt))
                    continue;
                int newMoveCost = currentTile.g_cost + GetDistance(currentTile.m_tilemapPosition, pos);

                if ( newMoveCost < mt.g_cost || !openSet.Contains(mt) )
                {
                    mt.g_cost = newMoveCost;
                    mt.h_cost = GetDistance(pos, end);
                    mt.parent = currentTile;

                    if(!openSet.Contains(mt))
                    {
                        openSet.Add(mt);
                    }
                }
            }
        }
    }

    List<Vector3> retracePath(MapTile start, MapTile end)
    {
        List<Vector3> path = new List<Vector3>();
        if (start == null || end == null)
            return path;

        MapTile current = end;

        while(current.GetInstanceID() != start.GetInstanceID())
        {
            Vector3 coords = m_gc.GetWorldCoordinatesCentreTile(current.m_tilemapPosition);
            path.Add(coords);
            current = current.parent;
        }

        path.Add(m_gc.GetWorldCoordinatesCentreTile(start.m_tilemapPosition));

        path.Reverse();
        return path;
    }

    int GetDistance(Vector3Int start, Vector3Int end)
    {
        int distX = Mathf.Abs(start.x - end.x);
        int distY = Mathf.Abs(start.y - end.y);

        if(distX > distY)
        {
            return distY + (distX - distY);
        }
        else
        {
            return distX + (distY - distX);
        }
    }

}
