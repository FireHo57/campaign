﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class GridController : MonoBehaviour, MouseHandler, GridRequestHandler
{
    // For some reason the cell position reported appears to be one higher than the 
    // cell at the position. I'm not sure why this is happening but current theories:
    //
    // 1) Cell center is actually a little below the grid map tile 
    // 2) Unity worldtocell position is BULLSHIT  
    static Vector3Int WORLD_TO_CELL_NUDGE=new Vector3Int(0,-1,0);
    static Vector3Int CELL_TO_WORLD_NUDGE=new Vector3Int(0,1,0);

    [SerializeField]
    Vector3 CentreTileNudge;

    private Vector3Int m_currentCell; 
    private Grid m_grid;
    private List<Vector3Int> m_currentHighlights;
    private Tilemap m_tilemap;

    private MapTile m_currentTile;
    List<GameObject> GridChangeTargets;
    
    public void Init(Grid grid)
    {
        m_grid = grid;
        m_tilemap = m_grid.GetComponentInChildren<Tilemap>();
        m_currentTile = null;
        GridChangeTargets = new List<GameObject>();
        m_currentCell = new Vector3Int(0,0,0);
        m_currentHighlights = new List<Vector3Int>();
    }

    public void RegisterGridChangeTarget(GameObject target)
    {
        GridChangeTargets.Add(target);
    }

    public void OnMousePosition(Vector3 position)
    {
        //highlight the cell at that position, dehighlight the cell that was highlighted
        Vector3Int cellCoordinates = GetCellCoordinates(position);
        
        MapTile tile = GetTile(cellCoordinates);
        if( tile != null )
        {
            if( tile != m_currentTile )
            {
                if( m_currentTile!= null )
                {
                    m_currentTile.Highlight(false);
                }
                
                m_currentTile = tile;
                m_currentTile.Highlight(true);
            }
        }

        if( !m_currentCell.Equals(cellCoordinates) )
        {
            //update the mouse coordinates
            m_currentCell = cellCoordinates; 
           
            foreach( GameObject go in GridChangeTargets)
            {
                ExecuteEvents.Execute<GridEventHandler>(go,null,(x,y)=>x.OnCoordinateChange(cellCoordinates));    
            }
            m_currentCell = cellCoordinates;
        }        
        
        m_tilemap.RefreshAllTiles();
    }

    public void RedrawCells(List<Vector3Int> cellsToRedraw)
    {
        foreach(Vector3Int cellPos in cellsToRedraw)
        {
            m_tilemap.RefreshTile(cellPos);
        }
    }

    public void OnMouseClick(int mouseButton)
    {
        foreach( GameObject go in GridChangeTargets)
        {
            ExecuteEvents.Execute<GridEventHandler>(go,null,(x,y)=>x.OnMouseClick(m_currentCell));    
        }
    }

    public void SetAtCoordinates(GameObject targetToSet, Vector3Int coordinates)
    {
        Vector3 worldPosition = GetWorldCoordinates(coordinates);
        targetToSet.transform.position = worldPosition;
    }

    public Vector3Int GetCellCoordinates(Vector3 worldPosition)
    {
        Vector3Int cellCoordinates = m_grid.WorldToCell(worldPosition);
        cellCoordinates.x-=1;
        return cellCoordinates;
    }

    public Vector3 GetWorldCoordinates(Vector3Int gridPosition)
    {
        Vector3 worldCoordinates = m_grid.CellToWorld(gridPosition);
        worldCoordinates+=CELL_TO_WORLD_NUDGE;
        return worldCoordinates;
    }

    public Vector3 GetWorldCoordinatesCentreTile(Vector3Int gridPosition)
    {
        Vector3 worldCoordinates = m_grid.CellToWorld(gridPosition);
        worldCoordinates += CELL_TO_WORLD_NUDGE;
        worldCoordinates += CentreTileNudge;
        return worldCoordinates;
    }

    public MapTile GetTile(Vector3Int cellCoordinate)
    {
        return (MapTile)m_tilemap.GetTile(cellCoordinate);
    }

    public List<Vector3Int> DisplayMove(int MoveValue, Vector3Int startPosition)
    {
        List<Vector3Int> moves = GetTile(startPosition).HighlightAvailableMoves(MoveValue);
        m_tilemap.RefreshAllTiles();
        return moves;
    }

    public delegate void Callback(Vector3Int pos);
    public void GetMove(int moveValue, Vector3Int startPosition, Callback endCallback)
    {
        m_currentHighlights = DisplayMove(moveValue, startPosition);
        IEnumerator waitCo = AwaitEndMove(endCallback);
        StartCoroutine(waitCo);
    }

    IEnumerator AwaitEndMove(Callback endCallback)
    {
        int i = 0;
        while(true)
        {
            i++;
            if(m_currentHighlights.Contains(m_currentCell))
            {
                if( Input.GetMouseButtonDown(0) )
                {
                    //move completed
                    endCallback(m_currentCell);
                    foreach(Vector3Int pos in m_currentHighlights)
                    {
                        Debug.Log("Highlighted position: "+pos);
                        MapTile mt = GetTile(pos);
                        mt.MoveHighlight(false);
                    }
                    m_tilemap.RefreshAllTiles();
                    yield return null;
                }
            }

            if(i > 6000)
            {
                yield return null;
            }
        }
    }
}
