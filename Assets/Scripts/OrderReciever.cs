﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface OrderReciever : IEventSystemHandler
{
    void StartOrder(string OrderName, GameObject OrderTarget);
    void EndOrder(Order OrderEnded);
}
