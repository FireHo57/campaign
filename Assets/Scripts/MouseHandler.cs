﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface MouseHandler : IEventSystemHandler
{
    void OnMousePosition(Vector3 position);

    void OnMouseClick(int mouseButton);
}
