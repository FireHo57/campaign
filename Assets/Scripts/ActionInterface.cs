﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ActionInterface
{
    void Run();

    void SetCallback(System.Action callbackFunction);
}
