﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField]
    float moveSpeed;

    float EPSILON = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");

        Vector3 xMove = new Vector3(0,0,0);
        Vector3 yMove = new Vector3(0,0,0);

        if( moveVertical > EPSILON || moveVertical < 0-EPSILON)
        {
            yMove = new Vector3(0,moveVertical,0);
        }

        if( moveHorizontal > EPSILON || moveHorizontal < 0-EPSILON )
        {
            xMove = new Vector3(moveHorizontal, 0, 0);
        }
    
        Vector3 moveVec = yMove+xMove;
        moveVec*=Time.deltaTime*moveSpeed;
        transform.position+=moveVec;
    }
}
