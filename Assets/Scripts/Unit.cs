﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Unit : MonoBehaviour
{
    public enum Attributes
    {
        MORALE = 0
    }

    [SerializeField]
    int m_move = 1;

    [SerializeField]
    int m_controlRadius = 1;

    [SerializeField]
    string m_name;

    [SerializeField]
    GameObject UnitCardPrefab;

    [SerializeField]
    List<string> AvailableOrders;

    [SerializeField]
    GameObject ShadowPrefab;


    public GameObject m_order = null;

    Vector3Int m_currentPosition;

    public void SetPosition(Vector3Int gridPosition)
    {
        m_currentPosition = gridPosition;
    } 

    public void RemoveOrder()
    {
        GameObject temp = m_order;
        m_order = null;
        Destroy(temp);
    }

    public void SetOrder(GameObject order)
    {
        if(m_order!= null)
        {
            RemoveOrder();
        }

        m_order = order;
        order.transform.parent = gameObject.transform;
    }

    public GameObject GetShadow()
    {
        // make the shadow
        GameObject shadow = GameObject.Instantiate(ShadowPrefab, gameObject.transform.position, gameObject.transform.rotation);
        // make the shadow transparent
        SpriteRenderer renderer = shadow.GetComponent<SpriteRenderer>();
        Color currentColor = renderer.material.color;
        currentColor.a = 0.5f;
        renderer.material.color = currentColor;
        return shadow;
    }

    public List<string> GetAvailableOrders()
    {
        return AvailableOrders;
    }

    public GameObject GetCard()
    {
        GameObject go = Instantiate(UnitCardPrefab);
        go.GetComponent<UnitCard>().Create(gameObject);
        return go;
    }

    public string GetName()
    {
        return m_name;
    }

    public int MoveValue()
    {
        return m_move;
    }

    public Vector3Int CurrentPosition()
    {
        return m_currentPosition;
    }

}
