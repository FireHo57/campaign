﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridBuilder : MonoBehaviour
{
    public static void CreateMap(Vector3Int minimum, Vector3Int maximum, Grid GridPrefab, TileBase[] TilePrefabs)
    {
        //Create the grid
        Grid grid = Instantiate(GridPrefab);
        Tilemap tilemap = grid.GetComponentInChildren<Tilemap>();
        tilemap.ClearAllTiles();
        
        for(int h_num = minimum.y; h_num < maximum.y; h_num++)
        {
            for(int w_num = minimum.x; w_num < maximum.x; w_num++)
            {
                Vector3Int pos = new Vector3Int(w_num, h_num, 0);
                MapTile mt = Instantiate(TilePrefabs[0]) as MapTile;
                mt.SetTilemapPosition(pos);
                //get it to calculate neighbours
                tilemap.SetTile(pos, mt);
            }
        }
    }
}
