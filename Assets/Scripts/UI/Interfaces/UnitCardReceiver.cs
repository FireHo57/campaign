﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface UnitCardReceiver : IEventSystemHandler
{
    void ShowUnitCard(GameObject unitCard, GameObject UnitController);
    void HideUnitCards();
}
