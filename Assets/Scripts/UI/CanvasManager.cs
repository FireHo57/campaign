﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Game Object for managing the canvas
public class CanvasManager : MonoBehaviour, UnitCardReceiver
{
    //helper struct for controlling of scrolling of cards
    struct ScrollInfo
    {

    }

    List<GameObject> VisibleUnitCards;
    GameObject CanvasGameObject;
    // Start is called before the first frame update
    void Start()
    {
        VisibleUnitCards = new List<GameObject>();
        gameObject.name = "Canvas"; 
        //make canvas
        gameObject.AddComponent<Canvas>();
        Canvas myCanvas = GetComponent<Canvas>();
        myCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        gameObject.AddComponent<CanvasScaler>();
        gameObject.AddComponent<GraphicRaycaster>();

        SetupUnitCardArea();
    }

    void SetupUnitCardArea()
    {
        GameObject unitArea = new GameObject();
        unitArea.name = "Unit Area";
        unitArea.AddComponent<RectTransform>();
        unitArea.AddComponent<CanvasRenderer>();
        unitArea.AddComponent<Image>();
        Canvas parentCanvas = gameObject.GetComponent<Canvas>();
        unitArea.transform.SetParent(gameObject.transform, false/*worldPositionStays*/);
        RectTransform parentCavasRect = parentCanvas.GetComponent<RectTransform>();
        RectTransform unitAreaRect = unitArea.GetComponent<RectTransform>();
        unitAreaRect.sizeDelta = new Vector2(parentCavasRect.rect.width, parentCavasRect.rect.height/5);
        
        unitAreaRect.anchorMin = new Vector2(0, 0);
        unitAreaRect.anchorMax = new Vector2(0, 0);
        unitAreaRect.pivot = new Vector2(0,0);

        //set the colour to be transparent
        unitArea.GetComponent<Image>().color = new Color32(0,0,0,0);   
    }

    public void ShowUnitCard(GameObject UnitCard, GameObject UnitController)
    {
        Debug.Log("Showing unit cards");
        //The start position should be the bottom left 
        // (+ however many cards have already been drawn)
        // putting the card at localposition 0, 0, 0 should do that 
        UnitCard.transform.SetParent(gameObject.transform, false);
        UnitCard.transform.localPosition = new Vector3(0,0,0);
        RectTransform unitCardRect = UnitCard.GetComponent<RectTransform>();
        unitCardRect.anchorMin = new Vector2(0, 0);
        unitCardRect.anchorMax = new Vector2(0, 0);
        unitCardRect.pivot = new Vector2(0,0);

        VisibleUnitCards.Add(UnitCard);
        //scroll card on from bottom of the screen  
    }

    public void HideUnitCards()
    {
        for( int i = 0; i < VisibleUnitCards.Count; i++ )
        {
            Destroy(VisibleUnitCards[i]);
        }   
        VisibleUnitCards = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
