using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class MoveUI : MonoBehaviour, GridEventHandler
{
    private List<Vector3Int> m_positions;
    private Vector3Int m_currentPosition;

    private LineRenderer m_lineRenderer;
    GameObject m_shadow;

    private GameObject m_grid;
    private bool m_fixed;

    private Vector3Int m_startPosition;

    public void Init(GameObject unit, GameObject gc)
    {
        m_grid = gc;
        m_grid.GetComponent<GridController>().RegisterGridChangeTarget(gameObject);
        m_startPosition = unit.GetComponent<Unit>().CurrentPosition();
        SetupLine();
        SetupShadow(unit);
    }

    private void SetupLine()
    {
        m_lineRenderer = gameObject.AddComponent<LineRenderer>();
        m_positions = new List<Vector3Int>();
        m_positions.Add(m_startPosition);
        // I want the colour to be a semi opaque red to signify this is what you've planned
        Color fadedRed = new Color();
        fadedRed = Color.red;
        fadedRed.a = 0.5f;
        //you can have colour gradients on lines but for now I just want a block colour
        m_lineRenderer.startColor = fadedRed;
        m_lineRenderer.endColor = fadedRed;
        m_lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        m_lineRenderer.widthMultiplier = 0.2f;
        // Need this next line because otherwise the line draws behind the grid
        /*MAGIC NUMBER ALERT*/
        m_lineRenderer.sortingOrder = 1;
    }

    private void SetupShadow(GameObject unit)
    {
        m_shadow = unit.GetComponent<Unit>().GetShadow();
        m_shadow.name = "Shadow";
        m_shadow.transform.parent = gameObject.transform;
        m_shadow.transform.position = unit.transform.position;
    }

    public void OnCoordinateChange(Vector3Int pos)
    {
        if(!m_fixed)
        {
            Debug.Log("OnCoordinateChanged: "+pos);
            m_currentPosition = pos;
            List<Vector3> path = m_grid.GetComponent<AStar>().GetPath(m_startPosition, pos);
            m_lineRenderer.positionCount = path.Count;
            m_lineRenderer.SetPositions(path.ToArray());
            transform.position = m_lineRenderer.GetPosition(m_lineRenderer.positionCount-1);
            m_shadow.transform.position = transform.position;
        }
    }

    public void OnMouseClick(Vector3Int pos)
    {
    }

    public void Fix()
    {
        m_fixed = true;
    }

    public void HideShadow()
    {
        m_shadow.SetActive(false);
    }

    public void HideLine()
    {
        m_lineRenderer.enabled = false;
    }

    public void HideAll()
    {
        HideShadow();
        HideLine();
    }
}